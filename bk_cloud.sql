/*
 Navicat Premium Data Transfer

 Source Server         : local-mysql
 Source Server Type    : MySQL
 Source Server Version : 80033
 Source Host           : localhost:3306
 Source Schema         : bk_cloud

 Target Server Type    : MySQL
 Target Server Version : 80033
 File Encoding         : 65001

 Date: 30/08/2023 15:19:18
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `title` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '用户账号',
  `pid` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '用户名称',
  `pids` json NULL COMMENT '群组',
  `status` smallint NULL DEFAULT 1 COMMENT '用户状态：1-正常；2-冻结；3-禁用；4-未激活；5-申诉',
  `created_at` timestamp(3) NULL DEFAULT NULL COMMENT '创建时间',
  `created_by` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '创建者',
  `updated_at` timestamp(3) NULL DEFAULT NULL COMMENT '最近一次修改的时间，也是乐观锁字段',
  `updated_by` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '最近一次修改的用户账号',
  `deleted_at` timestamp(3) NULL DEFAULT NULL COMMENT '删除时间，有值就表示已删除。默认是null',
  `deleted_by` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '删除者',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user_name`(`title` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 37 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '用户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '用户账号',
  `nick_name` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '用户名称',
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT 'aes密文',
  `role_id` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '主角色',
  `role_ids` json NULL COMMENT '角色合集',
  `group_ids` json NULL COMMENT '群组',
  `status` smallint NULL DEFAULT 1 COMMENT '用户状态：1-正常；2-冻结；3-禁用；4-未激活；5-申诉',
  `created_at` timestamp(3) NULL DEFAULT NULL COMMENT '创建时间',
  `created_by` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '创建者',
  `updated_at` timestamp(3) NULL DEFAULT NULL COMMENT '最近一次修改的时间，也是乐观锁字段',
  `updated_by` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '最近一次修改的用户账号',
  `deleted_at` timestamp(3) NULL DEFAULT NULL COMMENT '删除时间，有值就表示已删除。默认是null',
  `deleted_by` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '删除者',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user_name`(`user_name` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 37 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '用户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 'test', '测试员', NULL, 'admin', '[\"admin\", \"test\", \"itadmin\"]', '{\"a\": \"test\", \"b\": \"bbb\"}', 2, '2023-02-02 07:14:21.000', NULL, '2023-02-02 23:54:56.833', NULL, NULL, NULL);
INSERT INTO `sys_user` VALUES (2, 'bill', '罗先生', NULL, 'itadmin', '[\"a\", \"b\"]', '{\"a\": \"test\", \"b\": \"aaa\"}', 1, '2023-02-02 07:14:30.000', NULL, '2023-07-19 16:09:46.568', 'bill3', NULL, NULL);
INSERT INTO `sys_user` VALUES (4, 'edward', 'edward', NULL, 'boss', NULL, NULL, 1, '2023-07-22 23:46:16.013', '1111', '2023-07-22 23:46:16.013', '1111', NULL, NULL);
INSERT INTO `sys_user` VALUES (5, 'edward1', 'edward1', NULL, 'boss', NULL, NULL, 1, '2023-07-23 23:46:29.466', '1111', '2023-07-22 23:46:29.466', '1111', NULL, NULL);
INSERT INTO `sys_user` VALUES (6, 'admin', '管理员', NULL, 'boss', NULL, NULL, 1, '2023-07-23 23:46:34.105', '1111', '2023-07-22 23:46:34.105', '1111', NULL, NULL);
INSERT INTO `sys_user` VALUES (7, 'mask', '马斯克', NULL, 'boss', NULL, NULL, 1, '2023-07-24 23:47:16.283', '1111', '2023-07-22 23:47:16.283', '1111', NULL, NULL);
INSERT INTO `sys_user` VALUES (8, 'mazi', 'mazi', NULL, 'boss', NULL, NULL, 1, '2023-07-25 23:47:29.819', '1111', '2023-07-22 23:47:29.819', '1111', NULL, NULL);
INSERT INTO `sys_user` VALUES (9, 'dongbei', '东北', NULL, 'admin', NULL, NULL, 1, '2023-07-26 23:47:55.322', '1111', '2023-07-22 23:47:55.322', '1111', NULL, NULL);
INSERT INTO `sys_user` VALUES (10, 'abc', '张三', NULL, 'test', NULL, NULL, 1, '2023-07-26 23:48:38.903', '1111', '2023-07-22 23:48:38.903', '1111', NULL, NULL);
INSERT INTO `sys_user` VALUES (11, 'itadmin', '李四', NULL, 'test', NULL, NULL, 1, '2023-07-27 23:48:51.146', '1111', '2023-07-22 23:48:51.146', '1111', NULL, NULL);
INSERT INTO `sys_user` VALUES (12, 'mnopq', '张三丰', NULL, 'test', NULL, NULL, 1, '2023-07-22 23:48:58.482', '1111', '2023-07-22 23:48:58.482', '1111', NULL, NULL);
INSERT INTO `sys_user` VALUES (13, 'rst', '李立', NULL, 'test', NULL, NULL, 1, '2023-07-22 23:49:02.556', '1111', '2023-07-22 23:49:02.556', '1111', NULL, NULL);
INSERT INTO `sys_user` VALUES (14, 'uvw', '李立群', NULL, 'test', NULL, NULL, 1, '2023-07-22 23:49:06.305', '1111', '2023-07-22 23:49:06.305', '1111', NULL, NULL);
INSERT INTO `sys_user` VALUES (15, 'xyz', 'xyz', NULL, 'test', NULL, NULL, 1, '2023-07-22 23:49:11.255', '1111', '2023-07-22 23:49:11.255', '1111', NULL, NULL);
INSERT INTO `sys_user` VALUES (16, 'tmnw', 'tmnw', NULL, 'test', NULL, NULL, 1, '2023-07-22 23:49:15.865', '1111', '2023-07-22 23:49:15.865', '1111', NULL, NULL);
INSERT INTO `sys_user` VALUES (17, 'tmniw', 'tmniw', NULL, 'test', NULL, NULL, 1, '2023-07-22 23:49:20.690', '1111', '2023-07-22 23:49:20.690', '1111', NULL, NULL);
INSERT INTO `sys_user` VALUES (18, 'poiy', 'poiy', NULL, 'test', NULL, NULL, 1, '2023-07-22 23:49:25.234', '1111', '2023-07-22 23:49:25.234', '1111', NULL, NULL);
INSERT INTO `sys_user` VALUES (19, 'qwert', 'qwert', NULL, 'test', NULL, NULL, 1, '2023-07-22 23:49:30.834', '1111', '2023-07-22 23:49:30.834', '1111', '2023-08-29 22:05:58.000', NULL);
INSERT INTO `sys_user` VALUES (20, 'luokl', 'asdfg', NULL, 'test', NULL, NULL, 1, '2023-07-25 23:49:35.441', '1111', '2023-07-22 23:49:35.441', '1111', NULL, NULL);
INSERT INTO `sys_user` VALUES (21, 'zxcvb', 'zxcvb', NULL, 'test', NULL, NULL, 1, '2023-07-26 23:49:40.057', '1111', '2023-07-22 23:49:40.057', '1111', NULL, NULL);
INSERT INTO `sys_user` VALUES (22, 'yuiop', 'yuiop', NULL, 'test', NULL, NULL, 1, '2023-07-27 23:49:45.657', '1111', '2023-07-22 23:49:45.657', '1111', NULL, NULL);
INSERT INTO `sys_user` VALUES (23, 'hjklg', 'hjklg', NULL, 'test', NULL, NULL, 1, '2023-07-28 23:49:52.005', '1111', '2023-07-22 23:49:52.005', '1111', NULL, NULL);
INSERT INTO `sys_user` VALUES (24, 'mnrsic', 'mnrsic', NULL, 'test', '[\"admin1\", \"test\", \"itadmin\"]', NULL, 1, '2023-07-28 23:49:59.641', '1111', '2023-08-04 15:12:32.832', 'bill3', '2023-08-23 16:27:26.000', NULL);
INSERT INTO `sys_user` VALUES (25, 'hahali', '李哈暗', NULL, NULL, '[\"admin\", \"test\", \"itadmin\"]', NULL, 1, '2023-08-04 15:09:25.604', '23', '2023-08-23 16:41:22.236', 'blil', NULL, NULL);
INSERT INTO `sys_user` VALUES (26, 'abc', NULL, NULL, NULL, NULL, NULL, 1, '2023-08-22 18:03:35.269', 'blil', '2023-08-22 18:03:35.271', 'blil', NULL, NULL);
INSERT INTO `sys_user` VALUES (27, 'abolo', NULL, NULL, NULL, NULL, NULL, 1, '2023-08-22 18:04:21.209', 'blil', '2023-08-22 18:04:21.209', 'blil', NULL, NULL);
INSERT INTO `sys_user` VALUES (28, 'abolo', NULL, NULL, NULL, NULL, NULL, 1, '2023-08-22 18:04:40.466', 'blil', '2023-08-22 18:04:40.466', 'blil', NULL, NULL);
INSERT INTO `sys_user` VALUES (29, 'abolo', NULL, NULL, NULL, NULL, NULL, 1, '2023-08-22 18:04:59.866', 'blil', '2023-08-22 18:04:59.866', 'blil', NULL, NULL);
INSERT INTO `sys_user` VALUES (30, 'abolo', NULL, NULL, NULL, NULL, NULL, 1, '2023-08-22 18:05:02.433', 'blil', '2023-08-22 18:05:02.433', 'blil', NULL, NULL);
INSERT INTO `sys_user` VALUES (31, 'abolo', NULL, NULL, NULL, NULL, NULL, 1, '2023-08-22 18:05:03.988', 'blil', '2023-08-22 18:05:03.988', 'blil', NULL, NULL);
INSERT INTO `sys_user` VALUES (32, 'abolo', NULL, NULL, NULL, NULL, NULL, 1, '2023-08-22 18:05:05.386', 'blil', '2023-08-22 18:05:05.386', 'blil', NULL, NULL);
INSERT INTO `sys_user` VALUES (33, 'abolo1', NULL, NULL, NULL, NULL, NULL, 1, '2023-08-22 18:06:16.421', 'blil', '2023-08-22 18:06:16.423', 'blil', NULL, NULL);
INSERT INTO `sys_user` VALUES (34, 'siji', NULL, NULL, 'owner', NULL, NULL, 1, '2023-08-22 22:40:02.038', 'blil', '2023-08-22 23:01:20.103', 'blil', '2023-08-23 16:24:13.000', NULL);
INSERT INTO `sys_user` VALUES (35, 'siji', NULL, NULL, 'owner', NULL, NULL, 1, '2023-08-23 16:28:12.991', 'blil', '2023-08-23 16:29:09.549', 'blil', NULL, NULL);
INSERT INTO `sys_user` VALUES (36, 'laozi', NULL, NULL, NULL, NULL, NULL, 1, '2023-08-29 17:59:24.921', 'blil', '2023-08-29 17:59:24.926', 'blil', NULL, NULL);

-- ----------------------------
-- Table structure for sys_user_info
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_info`;
CREATE TABLE `sys_user_info`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `user_name` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `address` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `birthday` date NULL DEFAULT NULL COMMENT '年月日',
  `created_at` timestamp(3) NULL DEFAULT NULL,
  `updated_at` timestamp(3) NULL DEFAULT NULL,
  `deleted_at` timestamp(3) NULL DEFAULT NULL,
  `created_by` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `updated_by` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `deleted_by` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `is_delete` tinyint NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user_name`(`user_name` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 22 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_info
-- ----------------------------
INSERT INTO `sys_user_info` VALUES (8, 'bill', 'bill@qq.com', '13812345677', NULL, '2023-02-02 07:15:11.833', '2023-02-02 07:15:11.833', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_user_info` VALUES (9, 'itadmin', 'itadmin@qq.com', '13812345678', '2023-08-30', '2023-02-02 07:15:19.026', '2023-02-02 07:15:19.026', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_user_info` VALUES (10, 'luokl', 'luokl@qq.com', '13812345679', NULL, '2023-02-02 07:15:24.092', '2023-02-02 07:15:24.092', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_user_info` VALUES (11, 'lkl', 'lkl@qq.com', '13812345676', NULL, '2023-02-02 07:15:25.835', '2023-02-02 07:15:25.835', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_user_info` VALUES (12, 'siji', NULL, NULL, NULL, NULL, '2023-02-02 07:15:25.835', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_user_info` VALUES (13, 'siji', NULL, NULL, NULL, NULL, '2023-02-02 07:15:25.835', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_user_info` VALUES (14, 'siji', NULL, NULL, NULL, NULL, '2023-02-02 07:15:25.835', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_user_info` VALUES (15, 'siji', NULL, NULL, NULL, NULL, '2023-02-02 07:15:25.835', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_user_info` VALUES (16, 'siji', NULL, NULL, NULL, NULL, '2023-02-02 07:15:25.835', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_user_info` VALUES (17, 'siji', NULL, NULL, NULL, NULL, '2023-02-02 07:15:25.835', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_user_info` VALUES (18, 'laozi', NULL, NULL, NULL, NULL, '2023-02-02 07:15:25.835', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_user_info` VALUES (19, 'laozi', NULL, NULL, NULL, NULL, '2023-02-02 07:15:25.835', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_user_info` VALUES (20, 'mnopq', 'mnopq@qq.com', NULL, NULL, NULL, '2023-08-29 22:05:35.064', NULL, NULL, 'blil', NULL, 0);
INSERT INTO `sys_user_info` VALUES (21, 'edward', 'edward@qq.com', NULL, NULL, '2023-08-29 21:58:43.874', '2023-08-29 22:01:08.590', NULL, 'blil', 'blil', NULL, 0);

SET FOREIGN_KEY_CHECKS = 1;
