package com.luo.comm.config.interceptor;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;


/*
 *   拦截器的适配器
 * */
@Configuration
public class config implements WebMvcConfigurer {

    /*
    * 拦截规则：
    * /open/*  开放访问
      /md5/*  开放访问，但是需要验证MD5有效，否则拒绝。
      /rsa/*  开放访问，但是需要验证rsa有效,未配置以前全部拒绝
      其他，全部拦截，需要token验证。
    **/

    //设置默认需要开放的路由列表；
    public static final String[] excludePathList = new String[]{"/*", "/md5/**", "/open/**", "/demo/**"};

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 多个拦截器组成一个拦截器链

        // 添加MD5拦截
        registry.addInterceptor(new Md5Interceptor()).addPathPatterns("/md5/**");

        // 类添加MD5，添加更多拦截器，比如RSA拦截。

        // 添加统一拦截，除excludePath以外，都需要拦截。
        registry.addInterceptor(new AuthInterceptor()).addPathPatterns("/**").excludePathPatterns(excludePathList);

        // 拦截器挂载生效
        WebMvcConfigurer.super.addInterceptors(registry);
    }


}
