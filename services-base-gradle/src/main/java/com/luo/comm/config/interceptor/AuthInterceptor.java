package com.luo.comm.config.interceptor;


import com.luo.comm.entity.sys.SysUser;
import com.luo.comm.utils.ThreadLocal.ThreadlUser;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/*
 *  这是登陆拦截器，用于拦截所有请求，除特定接口外，都需要登陆，否则不允许访问。
 *  登陆的正常来说是在网关实现token的验证，然后将token中的用户信息写入请求头，这样本服务就可以获取了。
 *  如果是单机项目测试，直接在postman写入请求头即可。
 * */


public class AuthInterceptor implements HandlerInterceptor {



    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
         boolean reqAllow = false;
        // Controller方法处理之前执行
        // setHeader 只能在preHandle中处理，在其他地方处理无效
        // 约定所有请求走网关。网关处理后，需要将用户信息写入请求头。包括user_name,role_id
        String userName = request.getHeader("user_name");
        String roleId = request.getHeader("role_id");

        if (null !=userName && null !=roleId) {
            // 如果无网关，需在在这里增加token的校验。通过才放行。如果走的放的是网关，可以信任这里只要有就表示是合法，可以放许。
            // 这里示例，进行简化，全部放行。
            reqAllow = true;
            // 如果有用户信息，表示是正确的请求
            SysUser user = new SysUser();
            user.setUserName(userName);
            user.setRoleId(roleId);
            ThreadlUser.write(user);
        }

        // 如果 ALLOW 为真，放行。否则拒绝返回401。
        if (reqAllow) {
            // 如果为真，将相关用户信息写入

            return true;
        } else {
            response.setStatus(401);  //通过response设置返回信息，比如status设置状态
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write("401");
            return false;  //true 放行，false拒绝
        }

    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        // Controller方法处理完之后.
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        //  DispatcherServlet进行视图的渲染之后

    }

}
