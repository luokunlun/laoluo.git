package com.luo.comm.config;


import com.luo.comm.vo.BusinessException;
import com.luo.comm.vo.MyResEnum;
import com.luo.comm.vo.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
public class GlobalExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger("GlobalExceptionHandler");

    /**
     * 处理Controller层通用 Exception 异常
     * 可以防止Controller未处理异常的程序中止，进行全局捕获
     *
     * @ExceptionHandler(value = Exception.class) 可以定义多个。比如BusinessExceptionHandler,ServicesExceptionHandler
     */
    @ResponseBody
    @ExceptionHandler(value = Exception.class)
    public Result exceptionHandler(Exception e) {
        // 如果e.getMessage中包含了Cause: BusinessException，说明这是返回的一个业务异常。需要提取其中的msg进行处理。
        String msg = e.getMessage();
        int i = msg.lastIndexOf("Cause: BusinessException");
        if(i >0){
            msg= msg.substring(i + 24);
        }

        return Result.error(msg);
    }

    /**
     * 处理Controller层通用 BusinessException 异常
     * 可以防止Controller未处理异常的程序中止，进行全局捕获
     *
     * @ExceptionHandler(value = Exception.class) 可以定义多个。
     */
    @ResponseBody
    @ExceptionHandler(value = BusinessException.class)
    public Result BizExceptionHandler(BusinessException e) {
        return new Result(MyResEnum.BIZ_ERROR,null, e.getMsg());
    }

}
