package com.luo.comm.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.luo.comm.entity.sys.SysUser;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Component
@NoArgsConstructor
public class SysUserDto extends SysUser {

    @JsonInclude(JsonInclude.Include.NON_NULL) // 当为空时不转换JSON输出。这样前端就不会返回null.
    private String email;
}
