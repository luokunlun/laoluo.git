package com.luo.comm.controller.demo;

import com.luo.comm.services.AsyncService;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/demo/async")
@EnableAsync
public class AsyncServicesDemo {
    @Resource
    AsyncService asyncService;

    @RequestMapping(value = "/demo1", method = RequestMethod.GET)
    public String get1() {
        asyncService.hello();
        System.out.println("程序结束，但异步程序还在继续");
        return "RequestMapping with method=GET";
    }
}
