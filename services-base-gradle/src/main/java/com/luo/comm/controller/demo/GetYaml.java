package com.luo.comm.controller.demo;

import com.luo.comm.config.MyConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.yaml.snakeyaml.Yaml;

import javax.annotation.Resource;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

@RestController
@RequestMapping("/demo/yaml")
public class GetYaml {
    @Value("${my-config.oss}")
    private String osPath;

    @Resource
    private Environment environment;

    @Resource
    private MyConfig myPath;

    @GetMapping("/demo2")
    public String yamlDemo2() throws FileNotFoundException {
        // MyPath 是一个配置类，需要ConfigurationProperties、setter.
        System.out.println(myPath.getAnimal().get(1));
        return myPath.getFile();
    }

    ;

    @GetMapping("/demo3")
    public String yamlDemo3() {
        // environment 是系统自带的环境配置类
        String fileStr = environment.getProperty("my-path.file");
        System.out.println("/demo3:" + fileStr);

        return fileStr;
    }


}
