package com.luo.comm.controller.demo;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.HashMap;

@Controller
@RequestMapping("/demo/html")
public class ThymeleafDemo {

    @RequestMapping(value = "/table", method = RequestMethod.GET)
    public String table(Model model) {

        return "table";
    }

    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    public String hello(Model model) {
        // 传单个参数
        model.addAttribute("hello", "hello Thymeleaf！");

        // 通过map传参
        HashMap hashMap = new HashMap<>();
        hashMap.put("k1", "k1value");
        hashMap.put("k2", "k2value");
        hashMap.put("k3", "k3value");
        model.addAttribute("map", hashMap);

        return "hello";
    }



}
