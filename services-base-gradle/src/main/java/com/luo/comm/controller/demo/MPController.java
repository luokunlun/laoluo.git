package com.luo.comm.controller.demo;

import com.alibaba.fastjson2.JSONObject;
import com.luo.comm.entity.sys.SysUser;
import com.luo.comm.mapper.SysUserMapper;
import com.luo.comm.services.mp.sys.SysUserServiceImpl;
import com.luo.comm.vo.ReqBody;
import com.luo.comm.vo.Result;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;


/**
 * 由于拦截器设置的原因，demo目录是开放访问的，将目录调整为mp以便各服务中可以获取loginUser信息。
 * */
@RestController
@RequestMapping("/mp")
public class MPController {

    @Resource
    SysUserMapper sysUserMapper;

    @Resource
    SysUserServiceImpl userService;


    /**
     * MybatisPlus 通用save方法调用示例，传递一个实体类。
     */
    @PostMapping("/save")
    public Result save(@RequestBody SysUser user)  {
        // 如果save成功count会1，失败会是0。

        return userService.save(user);
    }

    /**
     * MybatisPlus 通用update方法调用示例，传递一个实体类。
     */


    @PostMapping("/update")
    public Result update(@RequestBody SysUser user)  {
        // 如果save成功count会1，失败会是0。
        return userService.update(user);
    }

    /**
     * MybatisPlus 通用findOne方法调用示例，传递一个实体类。
     * 参数支持条件为eq\like\between等通用条件转换。
     * ReqBody 需要传一个目标的实体类，可以是为空，会被body的同名属性替换。
     * 示例：{"select":"id,userName,nickName,status,updatedAt","size":5,"page":1,"between":[{"id":[3,3]}],"orderBy":[{"id":"asc"}]}
     */

    /**
     * MybatisPlus 通用update方法调用示例，传递一个实体类。
     */

    @PostMapping("/delete")
    public Result delete(@RequestBody JSONObject body)  {
        ReqBody<SysUser> reqBody = new ReqBody<>(new SysUser(),body);
        return userService.delete(reqBody);
    }


    @PostMapping("/find")
    public Result findOne(@RequestBody JSONObject body)  {
        ReqBody<SysUser> reqBody = new ReqBody<>(new SysUser(),body);
        return userService.find(reqBody);
    }

    /**
     * MybatisPlus 通用查找方法调用list示例，传递一个实体类。只支持单表查询。
     * 参数支持条件为eq\like\between等通用条件转换。
     * ReqBody 需要传一个目标的实体类，可以是为空，会被body的同名属性替换。
     * {"select":"id,userName,nickName,status,updatedAt","size":5,"page":1,"between":[{"id":[3,3]}],"orderBy":[{"id":"desc"}]}
     */

    @PostMapping("/list")
    public Result list(@RequestBody JSONObject body)  {
        // body包含了entity属性，以及附加属性（size,page,orderBy），其他的属性。
        // 将body转为按照ReqBody格式，附加属性以外的都装入标准实体类对象。将其他的无效字段过滤掉。
        ReqBody<SysUser> reqBody = new ReqBody<>(new SysUser(),body);

        return userService.list(reqBody);
    }

    /**
     * MybatisPlusJoin 通用findAll方法调用示例，传递一个实体类。支持多表联查。
     * 在list方法基础上，增加了join支持，入参参数相同。
     * 1、只支持一对一查询。dto配置相应的字段，如果是一对多，请用两次查询实现或其他自定义SQL实现。
     * 2、实现类中，是用dto对象查找和返回的，当做普通字段处理就行。比如user表关联userInfo表的email字段，查询时，直接{"select":"id,userName,email,roleIds","size":2,"page":2}
     * 注意，select方法是可选的，如果不传，则只返回主表对象的字段，并不会返回关联表对象的字段
     */

    @PostMapping("/findAll")
    public Result findAll(@RequestBody JSONObject body)  {
        ReqBody<SysUser> findBody = new ReqBody<>(new SysUser(),body);
        return userService.findAll(findBody);
    }




    /** getByNickName这是个性化的方法，具体SQL写在XML中，方法接口定义在mapper*/
    @PostMapping("/getByNickName")
    public Result test(@RequestBody JSONObject body)  {
        String nickName = body.getString("nickName");

       return nickName !=null ? Result.success(sysUserMapper.getUserByNickName(nickName)):Result.fail("nickName不能为空");
    }

}
