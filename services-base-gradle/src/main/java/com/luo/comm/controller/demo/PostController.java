package com.luo.comm.controller.demo;


import com.luo.comm.vo.BusinessException;
import com.luo.comm.vo.MyResEnum;
import com.luo.comm.vo.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;


/**
 * 如果用了RestController，会自动将入参转为json。直接用@RequestBody接收。
 * GET方法的入参也是适用的，像路由和问号传参也是可以用的。
 * 返回参数可以用任意类型，一般会封装，返回一个vo对象。
 * @return {
 * "code": 1000,
 * "data": 111111,
 * "msg": "成功"
 * }
 */

@RestController
@RequestMapping("/demo/post")
public class PostController {

    private final static Logger logger = LoggerFactory.getLogger("/Post"); // 创建Logger

    /**
     * ResultsObj返回示例1-标准方法ResultsObj.success、fail等
     *
     * @return {
     * "code": 1000,
     * "data": 111111,
     * "msg": "成功"
     * }
     */
    @PostMapping("/results1")
    public Result post5(@RequestBody HashMap map) {
        // @RequestParam 定义参数
        Integer i = 10, J = 0;
        try {
            J = i / (i - 10);
        } catch (Exception e) {
            throw new BusinessException(MyResEnum.NOT_ZERO);
        }

        return Result.success(J);
    }


    /**
     * ResultsObj返回示例2-根据code返回枚举类
     *
     * @return {
     * "code": 404,
     * "msg": "未找资资源"
     * }
     */
    @PostMapping("/results2")
    public Result post6(@RequestBody HashMap map) {

        return Result.byCode(404);
    }

    /**
     * ResultsObj返回示例3-根据name返回枚举类
     *
     * @return {
     * "code": 999,
     * "msg": "失败"
     * }
     */
    @PostMapping("/results3")
    public Result post7(@RequestBody HashMap map) {
        return new Result(MyResEnum.FAIL);
    }

    /**
     * ResultsObj返回示例3-根据name返回枚举类
     *
     * @return {
     * "code": 999,
     * "msg": "失败"
     * }
     */
    @PostMapping("/results4")
    public Result post8(@RequestBody HashMap map) {
        return new Result(MyResEnum.FAIL, map);
    }
}
