package com.luo.comm.controller;


import com.luo.comm.vo.Result;
import org.springframework.web.bind.annotation.*;


/**
 * 首页返回消息的POST处理，避免返回给调用者错误消息
 */

@RestController
public class HomeController {

    /**
     * POST方法返回
     */
    @PostMapping("/")
    public Result post() {
        return Result.success("欢迎访问！POST");
    }

    @GetMapping("/")
    public Result get() {
        return Result.success("欢迎访问！,请访问/index.html");
    }

}
