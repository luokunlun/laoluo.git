package com.luo.comm.controller.demo;

import com.luo.comm.services.dingding.SendMessageDing;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

@RestController
@RequestMapping("/demo/get")
public class GetController {

    @RequestMapping(value = "/type1", method = RequestMethod.GET)
    public String get1() {
        return "当前线程："+ Thread.currentThread().getId() +"userName:";
    }

    @GetMapping("/type2")
    public String get2() {
        SendMessageDing.sendMessage();
        return "GetMapping with method=GET OK！";
    }

    @GetMapping("/{id}/set")
    public String get3(@PathVariable String id) {
        // @PathVariable定义路径参数，并在Mapping中{id}定义参数位置
        return "path args " + id;
    }

    @GetMapping("/set")
    public String get4(@RequestParam(required = false) String key, @RequestParam String value) {
        // @RequestParam 定义参数
        return "path args " + key + ":" + value;
    }

    @GetMapping("/map")
    public HashMap get5() {
        // @RequestParam 定义参数
        HashMap map = new HashMap<>();
        map.put("aa", "aa1");
        map.put("bb", "bb1");
        return map;
    }
}
