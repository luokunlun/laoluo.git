package com.luo.comm.controller.sys;

import com.alibaba.fastjson2.JSONObject;
import com.luo.comm.entity.sys.SysMenu;
import com.luo.comm.services.mp.sys.SysMenuService;
import com.luo.comm.vo.ReqBody;
import com.luo.comm.vo.Result;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;


/**
 * 由于拦截器设置的原因，demo目录是开放访问的，将目录调整为mp以便各服务中可以获取loginUser信息。
 * */
@RestController
@RequestMapping("/sys/menu")
public class SysMenuController {


    @Resource
    SysMenuService sysMenuService;


    @PostMapping("/find")
    public Result findOne(@RequestBody JSONObject body)  {
        ReqBody<SysMenu> findBody = new ReqBody<>(new SysMenu(),body);
        return sysMenuService.findAll(findBody);
    }


}
