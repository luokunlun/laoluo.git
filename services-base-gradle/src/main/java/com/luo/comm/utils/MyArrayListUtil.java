package com.luo.comm.utils;

import java.util.LinkedList;
import java.util.List;

public class MyArrayListUtil {
    // 从 `ArrayList` 构造一个新的 `LinkedList` 的通用方法
    public static <T> LinkedList<T> getInstance(List<T> ArrayList)
    {
        LinkedList<T> linkedList = new LinkedList<>();

        for (T e: ArrayList) {
            linkedList.add(e);
        }

        return linkedList;
    }
}
