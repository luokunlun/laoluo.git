package com.luo.comm.utils;

import org.springframework.core.annotation.Order;


import com.luo.comm.App;

import java.util.Arrays;


/**
 * @author Bill
 * @description 获取Springboot 容器内的bean信息的工具类
 * App启动类中已经用了ac接收，可以直接使用ac= SpringApplication.run(App.class, args);
 */
@Order(99)  //
public class MySpringUtil {

    /**
     * 检查是否包含该bean
     */

    public static boolean containsBean(String beanName) {
        // className：com.luo.comm.entity.User
        // beanName：user
        try {
            return App.ac.containsBean(beanName);
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * 根据bean名字，返回bean类名
     */

    public static Class<?> getBeanClass(String beanName) {
        // className：com.luo.comm.entity.User
        // beanName：user
        try {

            return App.ac.getBean(beanName).getClass();
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 获取所有bean列表
     */

    public static String[] beanList() {
        String[] beans = App.ac.getBeanDefinitionNames();
        Arrays.sort(beans);
        return beans;
//        (beans);
//        for (String bean : beans)
//        {
//            System.out.println(bean + " of Type :: " + App.ac.getBean(bean).getClass());
//        }
    }

}

