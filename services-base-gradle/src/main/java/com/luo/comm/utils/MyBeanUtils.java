package com.luo.comm.utils;

import java.lang.reflect.Field;

public class MyBeanUtils {

    /**
     * 将驼峰式命名的字符串转换为下划线大写方式。如果转换前的驼峰式命名的字符串为空，则返回空字符串。
     * 例如：helloWorld->hello_world
     * @param name 转换前的驼峰式命名的字符串
     * @return 转换后下划线大写方式命名的字符串
     */
    public static String underscoreName(String name) {
        StringBuilder result = new StringBuilder();
        if (name != null && name.length() > 0) {
            // 将第一个字符处理成大写
            result.append(name.substring(0, 1).toLowerCase());
            // 循环处理其余字符
            for (int i = 1; i < name.length(); i++) {
                String s = name.substring(i, i + 1);
                // 在大写字母前添加下划线
                if (s.equals(s.toUpperCase()) && Character.isLetter(s.charAt(0))  && !Character.isDigit(s.charAt(0))) {
                    result.append("_");
                }
                // 原字符转成小写
                result.append(s.toLowerCase());
            }
        }
        return result.toString();
    }

    /** 判断实体类中是否含有某属性*/
    public static boolean hasAttribute(Object object, String attributeName) {
        Class<?> clazz = object.getClass();
        Field[] fields = clazz.getDeclaredFields();

        for (Field field : fields) {
            if (field.getName().equals(attributeName)) {
                return true;
            }
        }

        return false;
    }



}
