package com.luo.comm.utils.ThreadLocal;

import com.luo.comm.entity.sys.SysUser;
import org.springframework.core.NamedThreadLocal;

/**
 * UserThreadLocal用来保存当前线程的信息，比如当前请求的用户信息。
 * SpringBoot 每一次请求，都会产生一个新的线程。可以将需要线程共享的信息通过ThreadLocal进行存取。
 * 可以在拦截器或过滤器或GlobalExceptionHandler进行读取。
 * 本项目使用了拦截器，非开放路径都需要经由-AuthInterceptor处理，所以在AuthInterceptor中将用户信息保存。在其他地方可以使用。
 * 注意：开放接口未经过AuthInterceptor处理，是没有保存用户信息的，需要自行处理。
 * */

public class ThreadlUser {
    /**
     * 构造函数私有
     */
    private ThreadlUser() {
    }

    private static final ThreadLocal<SysUser> USER_INFO_THREAD_LOCAL =
            new NamedThreadLocal<>("loginUser");

    /**
     * 清除用户信息
     */
    public static void clear() {
        USER_INFO_THREAD_LOCAL.remove();
    }

    /**
     * 存储用户信息
     */
    public static void write(SysUser user) {
        USER_INFO_THREAD_LOCAL.set(user);
    }

    /**
     * 获取当前用户信息
     */
    public static SysUser read() {
        System.out.println("USER_INFO_THREAD_LOCAL:" + USER_INFO_THREAD_LOCAL);
        return USER_INFO_THREAD_LOCAL.get();
    }
}
