package com.luo.comm.services.mp.sys;


import org.springframework.stereotype.Component;

/**
 * SysUserInfoServiceImpl 是 SysUserInfoService 的实现类，
 * 其他service\controller调用这个方法，不要调用  SysUserService
 * 根据需要，重写服务中的相关方法。主要包括beforeSave beforeUpdate afterUpdate beforeFindOpHandle，具体见SysUserService
 * @author bill
 * @since 2023-08-27
 * */
@Component
public  class SysUserInfoServiceImpl extends SysUserInfoService {
    // 不需要的方法不用管。只需要覆写需要改写的方法

}
