package com.luo.comm.services.mp.sys;


import org.springframework.stereotype.Component;

/**
 * 在有需要的情况下，重写服务中的相关方法。
 * 主要包括beforeSave beforeUpdate afterUpdate beforeFindOpHandle，具体见SysUserService
 * @author bill
 * @since 2023-08-27
 * */
@Component
public  class SysMenuServiceImpl extends SysUserService {



}
