package com.luo.comm.services.mp.sys;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.luo.comm.entity.sys.SysUser;
import com.luo.comm.vo.BusinessException;
import com.luo.comm.vo.ConditionFields;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.LinkedHashMap;

/**
 * SysUserServiceImpl 是 SysUserService 的实现类，其他service\controller调用这个方法，不要调用  SysUserService
 * 根据需要，重写父类的相关方法。主要包括beforeSave beforeUpdate afterUpdate beforeFindOpHandle，具体见SysUserService
 * 不涉及的方法不用管
 * @author bill
 * @since 2023-08-27
 * */
@Component
public  class SysUserServiceImpl extends SysUserService {

    protected void beforeSave(SysUser entity){
        // userName不能为空
        if( StringUtils.isEmpty(entity.getUserName())){
            throw BusinessException.error("userName不能为空");
        }

        // userName不能和现有的重复。先这样写，后面find方法好了再覆写。
        QueryWrapper<SysUser> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_name",entity.getUserName());
        Long count = doMapper.selectCount(queryWrapper);
        if(count> 0){
            throw BusinessException.error("userName:"+entity.getUserName()+" 重复了");
        }
    };




    /** beforeFindOpHandle ,返回一个条件字段数组集合 ConditionFields
     * 如果子类不重写该方法，则返回默认的 new ConditionFields(),这个里面有默认的初始化数据，比如betweenFieldsArray等。
     * */
     protected ConditionFields beforeFindOpHandle(){
        // 添加条件字段对象，需要返回给调用者
         ConditionFields conditionFields = new ConditionFields();

         // ************ 定义需要连表查询的字段和关联条件***********
         // joinObj 的key 要和leftJoin里的面右表别名相同
         LinkedHashMap<String,Object> joinObj = new LinkedHashMap<>();
         LinkedHashMap<String,String> ui = new LinkedHashMap<>();
         ui.put("email","ui.email as email");
         ui.put("leftJoin","sys_user_info ui on t.user_name = ui.user_name");
         joinObj.put("ui",ui);
         // 定义其他的字段及连接处理，本文为了简单，统一使用的是leftJoin.

         // 将 joinObj 更新到conditionFields 返回
         conditionFields.setJoinObj(joinObj);

         // 返回 处理后的conditionFields
         return conditionFields;
    }

}
