package com.luo.comm.vo;

import lombok.Data;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 *  queryWrapper条件字段的转化，便于前调用，简化开发转换过程。
 *  多数是用于存字段名的，表示这些字段要用相对应的方法去转换,基本都是ArrayList。
 * */

@Data  // 省略了 setter\getter，但并不注入容器！
@Component  // 注入Spring容器，便于其他地方获取以检查是否合法bean
public class ConditionFields {

    /**
     * 精准查询字段，用eq去匹配.如果该字段没有被其他像like\between之类的使用到，则默认都是eq。所以不用写就行了
     * eg: ["id","roleId"]
     * 如果有一些字段被默认写成了其他处理方式，比如like处理，当某个方法需要精准查询时，需要配置到equalFieldsList。
     * */
    private ArrayList<String> equalFieldsList;

    /**
     * 模糊查询字段，用like去匹配
     * eg: ["userName","nickName"]，转换后的效果：queryWrapper.like("user_name","bill");
     * */
    private ArrayList<String> likeFieldsList;


    /**
     * 范围查询字段，用between去匹配，要求提供数组
     * eg: ["createdAt","updatedAt","deletedAt"],转换后的效果：queryWrapper.between("age", 18, 30)--->age between 18 and 30;
     * */
    private ArrayList<String> betweenFieldsList;

    /**
     * isnull字段，用isNull去匹配，要求提供数组
     * 转换为这样的数据：queryWrapper.isNull("name")--->name is null
     * */
    private ArrayList<String> isNullFieldsList;

    /**
     * isNotNull字段，用isNotNull去匹配，要求提供数组
     * 转换为这样的数据：queryWrapper.isNotNull("name")--->name is not null
     * */
    private ArrayList<String> isNotNullFieldsList;


    /**
     * in字段，用in去匹配，要求提供数组
     * 转换为这样的数据：queryWrapper.in("age", 1, 2, 3)--->age in (1,2,3)
     * */
    private ArrayList<String> inFieldsList;

    /**
     * select 要查询的字段，接收一个数组
     * 转换为这样的数据：queryWrapper.select("id", "name", "age")
     * */
    private ArrayList<String> selectList;

    /**
     * inSql,是一个where子句的意思。推荐用mpj,减少这种in子句。
     * 转换为这样的数据：queryWrapper.inSql("age", "1,2,3,4,5,6")--->age in (1,2,3,4,5,6)
     * inSql("id", "select id from table where id < 3")--->where id in (select id from table where id < 3)
     * */

    /**其他，比如having\groupby等，可以自定义，参见mybatisPlus官网，本系统只处理上述常用的字段列表，其他的暂时不用。
     * 特别复杂的功能，一般使用频率不高，可以用原生SQL的方法实现。
     * */
    private ArrayList<String> inSqlList;


    // Mpj联查查询专用对象，用于定义需要联表查询的表名称，关联字段
    private LinkedHashMap<String,Object> joinObj;


    /**无参构造方法，初始化一些数据，比如默认betweenFieldsList中添加时间字段*/
    public ConditionFields(){

        // likeFieldsList初始化，添加默认字段
        this.likeFieldsList = new ArrayList<>();

        // equalFieldsList初始化，添加默认字段
        this.equalFieldsList = new ArrayList<>();
        // 如果是按id查找的则推荐用find方法，是不需要走ConditionFields,但是findOne，findAll也支持，且id都会按eq处理
        // 这里也支持他，所以默认约定id都是eq条件。
        Collections.addAll(this.equalFieldsList,"id");

        // betweenFieldsList初始化，添加默认字段
        this.betweenFieldsList = new ArrayList<>();
        Collections.addAll(this.betweenFieldsList,"createdAt", "updatedAt", "deletedAt");

        // 添加joinObj 的默认字段
        this.setJoinObj(new LinkedHashMap<String,Object>());

    }


}
