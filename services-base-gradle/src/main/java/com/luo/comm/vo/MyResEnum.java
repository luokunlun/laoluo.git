package com.luo.comm.vo;

import lombok.Getter;

@Getter
public enum MyResEnum {

    // 枚举列表：
    /**
     * 999以前的，留给网关和服务器使用
     * 999是失败，1000成功。这个是固定且通用的返回。
     * 1000以后的，留给业务层封装使用
     * */

    FAIL(999, "失败"),
    SUCCESS(1000, "成功"),

    NOT_ZERO(1001, "不能输入0"),

    BIZ_ERROR(1002, "业务层异常"),

    NOT_EXIST(1003, "处理对象不存在"),

    ID_EMPTY(1004, "id不能为空"),
    USER_NO_LOGIN(1005, "用户未登陆或未正确获取用户信息"),
    UPDATE_NO_FIELDS(1006, "更新时，缺少可用字段"),

    /** 999以下的，原则上不建议使用，这个一般给网关用或系统预留*/
    NOTFOUND(404, "未找资资源");


    // 枚举内部变量，msg\code.
    private Integer code;
    private String msg;


    // 枚举类构造函数
    MyResEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    // 根据code返回枚举类。因于异常捕捉的原因，尽量少用。否则调用者需要自行处理IllegalArgumentException。
    public static MyResEnum byCode(Integer code) {
        for (MyResEnum statusEnum : MyResEnum.values()) {
            if (statusEnum.getCode().equals(code)) {
                //如果需要直接返回name则更改返回类型为String,return statusEnum.name;
                return statusEnum;
            }
        }
        throw new IllegalArgumentException("code is invalid");
    }

}
