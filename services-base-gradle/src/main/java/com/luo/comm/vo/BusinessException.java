package com.luo.comm.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.stereotype.Component;

@Data
@EqualsAndHashCode(callSuper=false)
@Component
public class BusinessException extends RuntimeException {


    private Integer code;
    private String msg;

    public BusinessException() {
        MyResEnum myResEnum = MyResEnum.BIZ_ERROR;
        this.code = myResEnum.getCode();
        this.msg = myResEnum.getMsg();
    }


    // 自定义返回业务异常消息
    public BusinessException(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    // 按resEnum定义好的枚举返回业务异常
    public BusinessException(MyResEnum myResEnum) {
        this.code = myResEnum.getCode();
        this.msg = myResEnum.getMsg();
    }

    // 通用业务异常类，返回一个error。统一返回MyResEnum.BIZERROR异常，但允许msg为自定义。
    public static BusinessException error(String msg) {
        MyResEnum myResEnum = MyResEnum.BIZ_ERROR;
        msg = !msg.isEmpty() ? msg : myResEnum.getMsg();
        return new BusinessException(myResEnum.getCode(), msg);
    }

}
