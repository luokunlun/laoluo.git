package com.luo.comm.entity.sys;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Date;

@Data  // 省略了 setter\getter，但并不注入容器！
@AllArgsConstructor  // 省略了有参的构造函数
@NoArgsConstructor // 省略了无参的构造函数。如果有写无参构造函数，会冲突
@Component  // 注入Spring容器，便于其他地方获取以检查是否合法bean
@TableName(autoResultMap = true)
public class SysUserInfo {

    /** ID主键 */
    private Long id;

    @JsonInclude(JsonInclude.Include.NON_NULL) // 当为空时不转换JSON输出。这样前端就不会返回null.
    private String userName;
    @JsonInclude(JsonInclude.Include.NON_NULL) // 当为空时不转换JSON输出。这样前端就不会返回null.
    private String email;
    @JsonInclude(JsonInclude.Include.NON_NULL) // 当为空时不转换JSON输出。这样前端就不会返回null.
    private String address;
    @JsonInclude(JsonInclude.Include.NON_NULL) // 当为空时不转换JSON输出。这样前端就不会返回null.
    private Date birthday;

    // 以下是通用字段 */
    /** 创建者*/
    @JsonInclude(JsonInclude.Include.NON_NULL) // 当为空时不转换JSON输出。这样前端就不会返回null.
    @TableField(fill = FieldFill.INSERT)
    private String createdBy;

    /** 创建时间*/
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSS")
    @JsonInclude(JsonInclude.Include.NON_NULL) // 当为空时不转换JSON输出。这样前端就不会返回null.
    @TableField(fill = FieldFill.INSERT)
    private Date createdAt;

    /** 更新者*/
    @JsonInclude(JsonInclude.Include.NON_NULL) // 当为空时不转换JSON输出。这样前端就不会返回null.
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String updatedBy;

    /** 更新时间*/
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSS")
    @JsonInclude(JsonInclude.Include.NON_NULL) // 当为空时不转换JSON输出。这样前端就不会返回null.
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @Version
    private Date updatedAt;


    /** 删除者*/
    @JsonInclude(JsonInclude.Include.NON_NULL) // 当为空时不转换JSON输出。这样前端就不会返回null.
    private String deletedBy;

    /** 删除时间*/
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSS")
    @JsonInclude(JsonInclude.Include.NON_NULL) // 当为空时不转换JSON输出。这样前端就不会返回null.
    @TableLogic
    private Date deletedAt;



}