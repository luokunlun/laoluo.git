package com.luo.comm.mapper;


import com.github.yulichang.base.MPJBaseMapper;
import com.luo.comm.entity.sys.SysUser;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/** 可以继承BaseMapper或MPJBaseMapper
 * 如果是个性化方法，支持xml配置，在resource文件mapper下面配置*/
@Mapper
public interface SysUserMapper extends MPJBaseMapper<SysUser> {

    List<SysUser> getUserByNickName(String userName);

}
